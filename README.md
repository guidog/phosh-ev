Phosh EV
========

Display electric vehicle information by querying the Vendor's public API
endpoints.

Currently only displays battery status for cars supported by [connect API][]
(e.g.  Hyundai, Kia) and NissanConnect API (Nissan).

This is for experimentation only.

Configuration
-------------
You need a file `accounts.ini` with the following content in ~/.config/phosh-ev/:

```ini
[general]
vendor = uvo
# vendor = nc

[simple_uvo]
region = 1
vendor = 1
username = username@example.org
password = the password
pin = 1234

[simple_nc]
region = EU
username = username@example.org
password = the password
```

The account currently needs to be created with the proprietary Vendor app (e.g.
https://play.google.com/store/apps/details?id=com.kia.connect.eu or https://play.google.com/store/apps/details?id=eu.nissan.nissanconnect.services). Use the
same information you provided there.

For vendor and regions check the [connect API][].

Development
-----------
Install dependencies:
```
sudo dnf install libadwaita-devel python3-devel # RedHat based
sudo apt install libadwaita-1-dev python3-dev # Debian based
pip install requests requests-oauthlib # For NissanConnect
```

To build:

```sh
make
```

Running
-------

```sh
make run
```

This needs the [connect API][] or [NissanConnect-HomeAssistant](https://github.com/dan-r/HomeAssistant-NissanConnect) installed in `../`. The result should look like this:

![Car charging](screenshots/charging.png)


Flatpak build
-------------
If in doubt where to intall the Python files you can also build and install a
flatpak:

```sh
make check-flatpak
```

or to crossbuild for a phone:

```sh
make arm64-flatpak
```

Rate limit
----------
The API is subject to rate limits:

  https://github.com/Hacksore/bluelinky/wiki/API-Rate-Limits

hence we only refresh every 5 minutes.

[connect API]: https://github.com/Hyundai-Kia-Connect/hyundai_kia_connect_api
