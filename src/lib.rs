use gtk::glib;
use simple_nc::{NcApi, NcAuth, NcVehicleInfo};
use simple_uvo::{UvoApi, UvoAuth, UvoVehicleInfo};

pub mod car_api;
pub mod simple_nc;
pub mod simple_uvo;

pub const LOG_DOMAIN: &str = "phosh-ev";

#[derive(Clone, Copy, Default)]
pub enum Vendor {
    #[default]
    Uvo,
    Nc,
}

#[derive(Clone, Copy)]
pub enum Api {
    Uvo(UvoApi),
    Nc(NcApi),
}

pub enum Auth {
    Uvo(UvoAuth),
    Nc(NcAuth),
}

pub enum VehicleInfo {
    Uvo(UvoVehicleInfo),
    Nc(NcVehicleInfo),
}

impl Default for Api {
    fn default() -> Self {
        todo!()
    }
}

impl Default for Auth {
    fn default() -> Self {
        todo!()
    }
}

pub fn parse_ini() -> (Vendor, Auth) {
    let key_file = glib::KeyFile::new();
    let mut filename = glib::user_config_dir();
    filename.push("phosh-ev");
    filename.push("accounts.ini");

    match key_file.load_from_file(filename.clone(), glib::KeyFileFlags::NONE) {
        Err(e) => glib::g_warning!(
            LOG_DOMAIN,
            "Can't read config {}: {}",
            filename.display(),
            e
        ),
        _ => (),
    }

    let vendor = match String::from(key_file.string("general", "vendor").unwrap()).as_str() {
        "uvo" => Vendor::Uvo,
        "nc" => Vendor::Nc,
        &_ => todo!(),
    };

    let auth: Auth = match vendor {
        Vendor::Uvo => Auth::Uvo(simple_uvo::UvoAuth {
            region: key_file.integer("simple_uvo", "region").unwrap(),
            brand: key_file.integer("simple_uvo", "vendor").unwrap(),
            username: String::from(key_file.string("simple_uvo", "username").unwrap()),
            password: String::from(key_file.string("simple_uvo", "password").unwrap()),
            /* TODO: pin should go into keyring */
            pin: String::from(key_file.string("simple_uvo", "pin").unwrap()),
        }),
        Vendor::Nc => Auth::Nc(simple_nc::NcAuth {
            region: String::from(key_file.string("simple_nc", "region").unwrap()),
            username: String::from(key_file.string("simple_nc", "username").unwrap()),
            password: String::from(key_file.string("simple_nc", "password").unwrap()),
        }),
    };

    return (vendor, auth);
}
