/*
 * Copyright (C) 2023 Guido Günther
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

use pyo3::{prelude::*, types::PyModule};
use std::fmt::{self, Display};
use std::sync::{Mutex, OnceLock};

use gtk::glib;

use serde::Deserialize;
use serde_json;

use crate::car_api::{CarApi, Feature};
use crate::{Auth, VehicleInfo};

const LOG_DOMAIN: &str = "simple_uvo";

#[derive(Debug, Clone)]
pub enum UvoError {
    PythonErr,
    ConvErr,
}

impl std::error::Error for UvoError {}

impl Display for UvoError {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            UvoError::PythonErr => formatter.write_str("Failure running python code"),
            UvoError::ConvErr => formatter.write_str("Failure converting data"),
        }
    }
}

impl From<PyErr> for UvoError {
    fn from(e: PyErr) -> Self {
        glib::g_warning!(LOG_DOMAIN, "Error in python code: {e:#?}");
        UvoError::PythonErr
    }
}

impl From<serde_json::Error> for UvoError {
    fn from(e: serde_json::Error) -> Self {
        glib::g_warning!(LOG_DOMAIN, "Error in json conversion: {e:#?}");
        UvoError::ConvErr
    }
}

#[derive(Default)]
pub struct UvoAuth {
    pub region: i32,
    pub brand: i32,
    pub username: String,
    pub password: String,
    pub pin: String,
}

#[derive(Debug, Deserialize)]
pub struct UvoVehicleInfo {
    pub vid: String,
    pub ev_battery_is_charging: bool,
    pub ev_battery_is_plugged_in: i32,
    pub bat_percentage: f64,
    pub current_charge_duration: i64,
    pub range: f64,

    pub loc_long: f64,
    pub loc_lat: f64,
}

#[derive(Clone, Copy)]
pub struct UvoApi {}

const HKCA: &str = r#"
import hyundai_kia_connect_api
import json


global _vm


def login(region, brand, username, password, pin):
  global _vm
  _vm = hyundai_kia_connect_api.VehicleManager(region, brand, username, password, pin)
  _vm.check_and_refresh_token()
  return True


def get_all():
  _vm.check_and_refresh_token()
  _vm.force_refresh_all_vehicles_states()
  _vm.update_all_vehicles_with_cached_state()

  for (vid, v) in _vm.vehicles.items():
    return json.dumps({'vid': vid,
                       'name': v.name,
                       'model': v.model,
                       'range': v.total_driving_range,
                       'current_charge_duration': v.ev_estimated_current_charge_duration,
                       'bat_percentage': v.ev_battery_percentage,
                       'ev_battery_is_charging': v.ev_battery_is_charging,
                       'ev_battery_is_plugged_in': v.ev_battery_is_plugged_in,
                       'loc_lat': v._location_latitude,
                       'loc_long': v._location_longitude,
                      })

def stop_charge(vid):
  _vm.stop_charge(vid)
  # https://github.com/Hyundai-Kia-Connect/hyundai_kia_connect_api/issues/424
  _vm.token = None

def start_charge(vid):
  _vm.start_charge(vid)
  # https://github.com/Hyundai-Kia-Connect/hyundai_kia_connect_api/issues/424
  _vm.token = None

"#;

fn uvo_context() -> &'static Mutex<Py<PyModule>> {
    static UVO_CONTEXT: OnceLock<Mutex<Py<PyModule>>> = OnceLock::new();
    UVO_CONTEXT.get_or_init(|| Mutex::new(init()))
}

pub fn init() -> Py<PyModule> {
    pyo3::prepare_freethreaded_python();
    Python::with_gil(|py| {
        PyModule::from_code(py, HKCA, "hkca.py", "hkca")
            .unwrap()
            .into_py(py)
    })
}

impl CarApi for UvoApi {
    fn login(&self, auth: Auth) -> Result<bool, Box<dyn std::error::Error>> {
        let uvo_auth = match auth {
            Auth::Uvo(a) => a,
            _ => panic!("Wrong Authentication type received"),
        };

        pyo3::prepare_freethreaded_python();

        Python::with_gil(|py| {
            let args = (
                uvo_auth.region,
                uvo_auth.brand,
                uvo_auth.username,
                uvo_auth.password,
                uvo_auth.pin,
            );

            let hkca = uvo_context().lock().unwrap();
            hkca.as_ref(py).getattr("login")?.call1(args)?;
            Ok(true)
        })
    }

    fn fetch_all(&self) -> Result<VehicleInfo, Box<dyn std::error::Error>> {
        pyo3::prepare_freethreaded_python();
        Python::with_gil(|py| {
            let hkca = uvo_context().lock().unwrap();
            let json: String = hkca.as_ref(py).getattr("get_all")?.call0()?.extract()?;
            glib::g_debug!(LOG_DOMAIN, "{}", json);
            Ok(VehicleInfo::Uvo(serde_json::from_str(&json)?))
        })
    }

    fn charge(&self, vid: &str, charge: bool) -> Result<bool, Box<dyn std::error::Error>> {
        let action: &str = if charge {
            "start_charge"
        } else {
            "stop_charge"
        };

        glib::g_debug!(LOG_DOMAIN, "{}", action);
        pyo3::prepare_freethreaded_python();
        Python::with_gil(|py| {
            let args = (vid,);
            let hkca = uvo_context().lock().unwrap();
            hkca.as_ref(py).getattr(action)?.call1(args)?;
            Ok(true)
        })
    }

    fn has_feature(&self, feature: Feature) -> bool {
        match feature {
            Feature::BatteryStatus => true,
            Feature::ClimateControl => true,
            Feature::Location => true,
            Feature::ChargeControl => true,
        }
    }
}
