/*
 * Copyright (C) 2023 Guido Günther
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
use phosh_ev::{
    car_api::CarApi, parse_ini, simple_nc, simple_uvo, Api, VehicleInfo, Vendor, LOG_DOMAIN,
};

use adw::subclass::prelude::*;
use adw::StatusPage;
use glib::subclass::InitializingObject;
use glib::{VariantDict, VariantTy};
use glib_macros::{clone, Properties};
use gtk::prelude::*;
use gtk::{gio, glib, Button, CompositeTemplate, Image, Label, LevelBar, ListBox, Spinner, Stack};

use std::cell::{Cell, OnceCell};

#[derive(CompositeTemplate, Properties, Default)]
#[template(resource = "/mobi/phosh/Ev/pev-window.ui")]
#[properties(wrapper_type = super::PevWindow)]
pub struct PevWindow {
    #[template_child]
    pub main_stack: TemplateChild<Stack>,

    /* page-ev */
    #[template_child]
    pub bat_icon: TemplateChild<Image>,
    #[template_child]
    pub bat_level: TemplateChild<Label>,
    #[template_child]
    pub range_label: TemplateChild<Label>,
    #[template_child]
    pub listbox_charging: TemplateChild<ListBox>,
    #[template_child]
    pub charge_bar: TemplateChild<LevelBar>,
    #[template_child]
    pub time_to_full: TemplateChild<Label>,
    #[template_child]
    pub charge_stack: TemplateChild<Stack>,
    #[template_child]
    pub loc_button: TemplateChild<Button>,
    #[template_child]
    pub updating_spinner: TemplateChild<Spinner>,

    /* page-error */
    #[template_child]
    pub error_status: TemplateChild<StatusPage>,

    pub vendor: OnceCell<Vendor>,
    pub api: OnceCell<Api>,

    pub vid: OnceCell<String>,
    pub needs_noti: Cell<bool>,

    #[property(get, set = Self::set_is_updating, explicit_notify)]
    pub is_updating: Cell<bool>,
    #[property(get, set = Self::set_is_charging, explicit_notify)]
    pub is_charging: Cell<bool>,

    pub bat_percentage: Cell<f64>,
    pub range: Cell<f64>,
}

#[glib::object_subclass]
impl ObjectSubclass for PevWindow {
    const NAME: &'static str = "PevWindow";
    type Type = super::PevWindow;
    type ParentType = adw::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();

        klass.install_action("win.stop-charge", None, move |obj, _, _| {
            glib::g_debug!(LOG_DOMAIN, "win.stop-charge");
            obj.imp().charge(false);
        });

        klass.install_action("win.start-charge", None, move |obj, _, _| {
            glib::g_debug!(LOG_DOMAIN, "win.start-charge");
            obj.imp().charge(true);
        });

        klass.install_action("win.refresh", None, move |obj, _, _| {
            glib::g_debug!(LOG_DOMAIN, "win.refresh");
            obj.imp().poll_status();
        });

        klass.install_action(
            "win.open-geo",
            Some(VariantTy::STRING),
            move |obj, _, param| {
                glib::g_debug!(LOG_DOMAIN, "win.open-geo");
                if let Some(loc) = param.and_then(String::from_variant) {
                    obj.imp().open_geo(&loc);
                }
            },
        );
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

#[glib::derived_properties]
impl ObjectImpl for PevWindow {
    fn constructed(&self) {
        // Call "constructed" on parent
        self.parent_constructed();

        let obj = self.obj();
        obj.bind_property("is-updating", &self.updating_spinner.get(), "spinning")
            .sync_create()
            .build();

        obj.action_set_enabled("win.refresh", false);
        self.login();

        let app = gio::Application::default()
            .expect("Failed to retrieve application singleton")
            .downcast::<gtk::Application>()
            .unwrap();
        // Make sure Phosh's lockscreen sees up to date values
        app.connect_screensaver_active_notify(clone!(
            #[weak(rename_to = window)]
            self,
            move |_app| {
                window.notify_dbus();
            }
        ));
    }
}

impl WidgetImpl for PevWindow {}
impl WindowImpl for PevWindow {}
impl ApplicationWindowImpl for PevWindow {}
impl AdwApplicationWindowImpl for PevWindow {}

glib::wrapper! {
    pub struct Window(ObjectSubclass<PevWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl PevWindow {
    const SLEEP_SECONDS: u32 = 300;
    const NOTI_TIME_TO_FULL: i64 = 600;
    const NOTI_FULL_LEVEL: f64 = 80.0;

    fn bat_icon(&self, percentage: f64, charging: bool) -> String {
        let icon_name;
        let level = (percentage / 10.0) as i64 * 10;
        match charging {
            true => icon_name = format!("battery-level-{}-charging-symbolic", level),
            false => icon_name = format!("battery-level-{}-symbolic", level),
        }
        icon_name
    }

    fn format_duration(&self, minutes: i64) -> String {
        match minutes {
            0..=59 => format!("{}m", minutes),
            _ => format!("{}h {}m", minutes / 60, minutes % 60),
        }
    }

    fn update_vehicle_status(&self, info: &VehicleInfo) {
        match self.vendor.get().clone().unwrap() {
            Vendor::Uvo => {
                if let VehicleInfo::Uvo(info) = info {
                    let vid = self.vid.get_or_init(|| info.vid.clone());
                    if !vid.eq(&info.vid) {
                        glib::g_critical!(
                            LOG_DOMAIN,
                            "Got info for wrong vehicle {}, ignoring",
                            &info.vid
                        );
                        return;
                    }
                    self.bat_level
                        .set_label(&format!("{}%", info.bat_percentage));
                    let icon_name = self.bat_icon(info.bat_percentage, info.ev_battery_is_charging);
                    self.bat_icon.set_icon_name(Some(&icon_name));
                    self.range_label.set_label(&format!("{:.0} km", info.range));

                    let is_charging = info.ev_battery_is_charging;
                    let is_plugged_in: bool = info.ev_battery_is_plugged_in > 0;

                    let time_left: String = match (is_charging, is_plugged_in) {
                        (true, _) => {
                            format!(
                                "{} until charged",
                                self.format_duration(info.current_charge_duration)
                            )
                        }
                        (false, true) => String::from("Plugged in, not charging"),
                        (_, _) => String::from("Charging duration unknown"),
                    };

                    self.charge_stack.set_visible_child_name(match is_charging {
                        true => "page-stop-charge",
                        false => "page-start-charge",
                    });
                    self.time_to_full.set_label(&time_left);
                    self.charge_bar.set_value(info.bat_percentage);
                    self.listbox_charging
                        .set_visible(is_charging || is_plugged_in);
                    self.main_stack.set_visible_child_name("page-ev");

                    self.obj()
                        .action_set_enabled("win.start-charge", is_plugged_in && !is_charging);
                    self.obj()
                        .action_set_enabled("win.stop-charge", is_charging);

                    let loc = &format!("geo:{},{}", info.loc_lat, info.loc_long);
                    self.loc_button
                        .set_action_target_value(Some(&loc.to_variant()));

                    self.notify(
                        is_charging,
                        info.current_charge_duration,
                        info.bat_percentage,
                    );

                    self.set_is_charging(is_charging);

                    self.range.replace(info.range);
                    self.bat_percentage.replace(info.bat_percentage);

                    self.notify_dbus();
                } else {
                    panic!("Unknown VehicleInfo received")
                }
            }
            Vendor::Nc => {
                if let VehicleInfo::Nc(info) = info {
                    let vid = self.vid.get_or_init(|| info.vid.clone());
                    if !vid.eq(&info.vid) {
                        glib::g_critical!(
                            LOG_DOMAIN,
                            "Got info for wrong vehicle {}, ignoring",
                            &info.vid
                        );
                        return;
                    }
                    self.bat_level
                        .set_label(&format!("{}%", info.bat_percentage));
                    let icon_name = self.bat_icon(info.bat_percentage, info.ev_battery_is_charging);
                    self.bat_icon.set_icon_name(Some(&icon_name));
                    self.range_label.set_label(&format!("{:.0} km", info.range));

                    let is_charging = info.ev_battery_is_charging;
                    let is_plugged_in: bool = info.ev_battery_is_plugged_in > 0;

                    let time_left: String = match (is_charging, is_plugged_in) {
                        (true, _) => {
                            format!(
                                "{} until charged",
                                self.format_duration(info.current_charge_duration)
                            )
                        }
                        (false, true) => String::from("Plugged in, not charging"),
                        (_, _) => String::from("Charging duration unknown"),
                    };

                    self.charge_stack.set_visible_child_name(match is_charging {
                        true => "page-stop-charge",
                        false => "page-start-charge",
                    });
                    self.time_to_full.set_label(&time_left);
                    self.charge_bar.set_value(info.bat_percentage);
                    self.listbox_charging
                        .set_visible(is_charging || is_plugged_in);
                    self.main_stack.set_visible_child_name("page-ev");

                    self.obj()
                        .action_set_enabled("win.start-charge", is_plugged_in && !is_charging);
                    self.obj()
                        .action_set_enabled("win.stop-charge", is_charging);

                    self.notify(
                        is_charging,
                        info.current_charge_duration,
                        info.bat_percentage,
                    );

                    self.set_is_charging(is_charging);

                    self.range.replace(info.range);
                    self.bat_percentage.replace(info.bat_percentage);

                    self.notify_dbus();
                } else {
                    panic!("Unknown VehicleInfo received")
                }
            }
        }
    }

    fn notify(&self, is_charging: bool, time_to_full: i64, bat_percentage: f64) {
        let obj = self.obj();

        glib::g_debug!(
            LOG_DOMAIN,
            "Checking notification: was_charging: {}, is_charging: {}, {}m, {}%",
            self.is_charging.get(),
            is_charging,
            time_to_full,
            bat_percentage
        );

        /* Charging started */
        if !self.is_charging.get() && is_charging {
            let noti = gio::Notification::new("Battery charging");
            noti.set_body(Some(&format!(
                "Battery fully charged in {}",
                self.format_duration(time_to_full)
            )));
            obj.application()
                .unwrap()
                .send_notification(Some("charge-progress"), &noti);
            return;
        }

        /* Battery fully charged, charging stopped */
        if self.is_charging.get() && !is_charging && bat_percentage >= 100.0 {
            let noti = gio::Notification::new("Battery charged");
            noti.set_body(Some(&format!("Battery is now fully charged")));
            obj.application()
                .unwrap()
                .send_notification(Some("charge-progress"), &noti);
            self.needs_noti.set(false);
            return;
        }

        if !self.needs_noti.get() {
            return;
        }

        if is_charging && time_to_full >= PevWindow::NOTI_TIME_TO_FULL {
            /* Battery almost full */
            let noti = gio::Notification::new("Battery almost full");
            noti.set_body(Some(&format!(
                "Battery fully charged in {}",
                self.format_duration(time_to_full)
            )));
            obj.application()
                .unwrap()
                .send_notification(Some("charge-progress"), &noti);
            self.needs_noti.set(false);
            return;
        }

        if self.is_charging.get() && bat_percentage >= PevWindow::NOTI_FULL_LEVEL {
            /* Battery was charging and crossed the threshold (charging might have already stopped) */
            let noti =
                gio::Notification::new(&format!("Battery above {}%", PevWindow::NOTI_FULL_LEVEL));
            noti.set_body(Some(&format!(
                "Battery fully charged in {} minutes",
                time_to_full
            )));
            self.needs_noti.set(false);
            return;
        }
    }

    fn show_error_page(&self, msg: String) {
        self.error_status.set_title(&msg);
        self.main_stack.set_visible_child_name("page-error");
    }

    fn open_geo(&self, loc: &str) {
        let win = self.obj().application().unwrap().active_window().unwrap();
        let launcher = gtk::UriLauncher::new(&loc);

        launcher.launch(Some(&win), gio::Cancellable::NONE, move |_| {});
    }

    fn login(&self) {
        let (sender, receiver) = async_channel::bounded(1);
        gio::spawn_blocking(move || {
            let (vendor, auth) = parse_ini();
            let api: Api = match vendor {
                Vendor::Uvo => Api::Uvo(simple_uvo::UvoApi {}),
                Vendor::Nc => Api::Nc(simple_nc::NcApi {}),
            };
            let ret = match api {
                Api::Uvo(uvo) => uvo.login(auth).unwrap(),
                Api::Nc(nc) => nc.login(auth).unwrap(),
            };
            sender
                .send_blocking((vendor, api, ret))
                .expect("Could not send");
        });

        glib::spawn_future_local(clone!(
            #[weak(rename_to = this)]
            self,
            async move {
                while let Ok((vendor, api, ret)) = receiver.recv().await {
                    match ret {
                        true => {
                            this.vendor.get_or_init(|| vendor);
                            this.api.get_or_init(|| api);
                            glib::g_debug!(LOG_DOMAIN, "Logged in");
                            this.obj().action_set_enabled("win.refresh", true);
                            this.poll_status_when_idle();
                            this.poll_status_continously();
                        }
                        false => {
                            glib::g_warning!(LOG_DOMAIN, "Error logging in");
                            this.show_error_page(format!("Failed to log in"));
                        }
                    };
                }
            }
        ));
    }

    fn poll_status(&self) {
        self.set_is_updating(true);
        glib::spawn_future_local(clone!(
            #[weak(rename_to = this)]
            self,
            async move {
                let vehicle_info = match this.api.get().clone() {
                    Some(Api::Uvo(uvo)) => uvo.fetch_all(),
                    Some(Api::Nc(nc)) => nc.fetch_all(),
                    None => todo!(),
                };
                this.set_is_updating(false);
                match vehicle_info {
                    Ok(info) => this.update_vehicle_status(&info),
                    Err(_) => {
                        this.show_error_page(format!("Failed to get data"));
                    }
                };
            }
        ));
    }

    fn poll_status_when_idle(&self) {
        glib::idle_add_local_once(clone!(
            #[weak(rename_to = this)]
            self,
            move || {
                this.poll_status();
            }
        ));
    }

    fn poll_status_continously(&self) {
        glib::timeout_add_seconds_local(
            PevWindow::SLEEP_SECONDS,
            clone!(
                #[weak(rename_to = this)]
                self,
                #[upgrade_or]
                glib::ControlFlow::Break,
                move || {
                    this.poll_status();
                    glib::ControlFlow::Continue
                }
            ),
        );
    }

    fn charge(&self, charge: bool) {
        /* Stop charge can take ages, disable until next poll */
        self.obj().action_set_enabled("win.stop-charge", false);
        self.obj().action_set_enabled("win.start-charge", false);

        glib::spawn_future_local(clone!(
            #[weak(rename_to = this)]
            self,
            async move {
                let vid = this.vid.get().unwrap().clone();
                let _ = match this.api.get().clone() {
                    Some(Api::Uvo(uvo)) => uvo.charge(&vid, charge),
                    Some(Api::Nc(nc)) => nc.charge(&vid, charge),
                    None => todo!(),
                };
            }
        ));
    }

    fn set_is_updating(&self, is_updating: bool) {
        if is_updating != self.is_updating.replace(is_updating) {
            self.obj().notify("is-updating");
        }
    }

    fn set_is_charging(&self, is_charging: bool) {
        if is_charging != self.is_charging.replace(is_charging) {
            if !is_charging {
                self.needs_noti.set(true);
            }
            self.obj().notify("is-charging");
        }
    }

    pub fn notify_dbus(&self) {
        let conn = gio::bus_get_sync(gio::BusType::Session, None::<&gio::Cancellable>).unwrap();

        let dict = VariantDict::new(None);
        dict.insert("progress-visible", true);
        dict.insert("count-visible", true);
        dict.insert("progress", self.bat_percentage.get() / 100.0);
        dict.insert("count", self.range.get().round() as i64);

        let variant = ("application://mobi.phosh.Ev.desktop", dict).to_variant();
        let result = conn.emit_signal(
            None,
            "/mobi/phosh/EV",
            "com.canonical.Unity.LauncherEntry",
            "Update",
            Some(&variant),
        );
        match result {
            Ok(_) => {}
            Err(e) => {
                glib::g_warning!(LOG_DOMAIN, "Error sending dbus signal in: {e:#?}");
            }
        };
    }
}
