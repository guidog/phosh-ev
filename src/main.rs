/*
 * Copyright (C) 2023 Guido Günther
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
mod window;

use adw::Application;
use glib::clone;
use gtk::prelude::*;
use gtk::{gio, glib};
use window::PevWindow;

const APP_ID: &str = "mobi.phosh.Ev";

fn main() -> glib::ExitCode {
    gio::resources_register_include!("mobi.phosh.Ev.gresource")
        .expect("Failed to register resources.");

    let app = Application::builder().application_id(APP_ID).build();
    app.connect_activate(activate);
    app.run()
}

fn activate(app: &Application) {
    match app.active_window() {
        Some(win) => win.present(),
        _ => setup(app),
    }
}

fn setup(app: &Application) {
    let window = PevWindow::new(app);

    let action_about = gio::SimpleAction::new("about", None);
    action_about.connect_activate(clone!(move |_, _| {
        let about = adw::AboutWindow::from_appdata(
            &format!("/mobi/phosh/Ev/{}.metainfo.xml", APP_ID),
            None,
        );
        about.present();
    }));
    window.add_action(&action_about);

    window.present();
}
