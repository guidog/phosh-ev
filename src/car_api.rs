/*
 * Copyright (C) 2024 Federico Amedeo Izzo
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Federico Amedeo Izzo <federico@izzo.pro>
 */

use crate::{Auth, VehicleInfo};

pub trait CarApi {
    fn login(&self, auth: Auth) -> Result<bool, Box<dyn std::error::Error>>;
    fn fetch_all(&self) -> Result<VehicleInfo, Box<dyn std::error::Error>>;
    fn charge(&self, vid: &str, charge: bool) -> Result<bool, Box<dyn std::error::Error>>;
    fn has_feature(&self, feature: Feature) -> bool;
}

pub enum Feature {
    BatteryStatus,
    ClimateControl,
    Location,
    ChargeControl,
}
