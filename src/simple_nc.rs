/*
 * Copyright (C) 2024 Federico Amedeo Izzo
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Federico Amedeo Izzo <federico@izzo.pro>
 */

use pyo3::{prelude::*, types::PyModule};
use std::fmt::{self, Display};
use std::panic;
use std::sync::{Mutex, OnceLock};

use gtk::glib;

use serde::Deserialize;
use serde_json;

use crate::car_api::{CarApi, Feature};
use crate::{Auth, VehicleInfo};

const LOG_DOMAIN: &str = "simple_nc";

#[derive(Debug, Clone)]
pub enum NcError {
    PythonErr,
    ConvErr,
}

impl std::error::Error for NcError {}

impl Display for NcError {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            NcError::PythonErr => formatter.write_str("Failure running python code"),
            NcError::ConvErr => formatter.write_str("Failure converting data"),
        }
    }
}

impl From<PyErr> for NcError {
    fn from(e: PyErr) -> Self {
        glib::g_warning!(LOG_DOMAIN, "Error in python code: {e:#?}");
        NcError::PythonErr
    }
}

impl From<serde_json::Error> for NcError {
    fn from(e: serde_json::Error) -> Self {
        glib::g_warning!(LOG_DOMAIN, "Error in json conversion: {e:#?}");
        NcError::ConvErr
    }
}

#[derive(Clone, Default)]
pub struct NcAuth {
    pub region: String,
    pub username: String,
    pub password: String,
}

#[derive(Debug, Deserialize)]
pub struct NcVehicleInfo {
    pub vid: String,
    pub ev_battery_is_charging: bool,
    pub ev_battery_is_plugged_in: i32,
    pub bat_percentage: f64,
    pub current_charge_duration: i64,
    pub range: f64,
}

#[derive(Clone, Copy)]
pub struct NcApi {}

const NISSAN_CONNECT: &str = r#"
import json
import sys, os
from unittest.mock import MagicMock
# NOTE: https://stackoverflow.com/questions/7505988/importing-from-a-relative-path-in-python
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'HomeAssistant-NissanConnect'))
# Avoid homeassistant dependency
sys.modules['homeassistant'] = MagicMock()
sys.modules['homeassistant.helpers'] = MagicMock()
sys.modules['homeassistant.helpers.update_coordinator'] = MagicMock()

from custom_components.nissan_connect import NCISession

global session

def login(region, username, password):
  global session
  session = NCISession(region)
  session.login(username, password)

def get_all():
  for v in session.fetch_vehicles():
    v.refresh()
    return json.dumps({'vid': v.vin,
                       'name': v.nickname,
                       'model': v.model_name,
                       'range': v.range_hvac_on,
                       'current_charge_duration': v.charge_time_required_to_full[v.charging_speed],
                       'bat_percentage': v.battery_level,
                       'ev_battery_is_charging': bool(v.charging.value),
                       'ev_battery_is_plugged_in': v.plugged_in.value,
                       'loc_lat': 0,
                       'loc_long': 0,
                      })

def stop_charge(vid):
  pass

def start_charge(vid):
  pass

"#;

fn nc_context() -> &'static Mutex<Py<PyModule>> {
    static NC_CONTEXT: OnceLock<Mutex<Py<PyModule>>> = OnceLock::new();
    NC_CONTEXT.get_or_init(|| Mutex::new(init()))
}

pub fn init() -> Py<PyModule> {
    pyo3::prepare_freethreaded_python();
    Python::with_gil(|py| {
        PyModule::from_code(py, NISSAN_CONNECT, "nissan_connect.py", "nissan_connect")
            .unwrap()
            .into_py(py)
    })
}

impl CarApi for NcApi {
    fn login(&self, auth: Auth) -> Result<bool, Box<dyn std::error::Error>> {
        let nc_auth = match auth {
            Auth::Nc(a) => a,
            _ => panic!("Wrong Authentication type received"),
        };

        pyo3::prepare_freethreaded_python();

        Python::with_gil(|py| {
            let args = (nc_auth.region, nc_auth.username, nc_auth.password);

            let nissan_connect = nc_context().lock().unwrap();
            nissan_connect.as_ref(py).getattr("login")?.call1(args)?;
            Ok(true)
        })
    }

    fn fetch_all(&self) -> Result<VehicleInfo, Box<dyn std::error::Error>> {
        pyo3::prepare_freethreaded_python();
        Python::with_gil(|py| {
            let nissan_connect = nc_context().lock().unwrap();
            let json: String = nissan_connect
                .as_ref(py)
                .getattr("get_all")?
                .call0()?
                .extract()?;
            glib::g_debug!(LOG_DOMAIN, "{}", json);
            Ok(VehicleInfo::Nc(serde_json::from_str(&json)?))
        })
    }

    fn charge(&self, vid: &str, charge: bool) -> Result<bool, Box<dyn std::error::Error>> {
        let action: &str = if charge {
            "start_charge"
        } else {
            "stop_charge"
        };

        glib::g_debug!(LOG_DOMAIN, "{}", action);
        pyo3::prepare_freethreaded_python();
        Python::with_gil(|py| {
            let args = (vid,);
            let nissan_connect = nc_context().lock().unwrap();
            nissan_connect.as_ref(py).getattr(action)?.call1(args)?;
            Ok(true)
        })
    }

    fn has_feature(&self, feature: Feature) -> bool {
        match feature {
            Feature::BatteryStatus => true,
            Feature::ClimateControl => false,
            Feature::Location => false,
            Feature::ChargeControl => false,
        }
    }
}
