(
 (rust-mode . (
            (indent-tabs-mode . nil)
            (rust-indet-offset . 4)
            ))
 (setq auto-mode-alist (cons '("\\.ui$" . nxml-mode) auto-mode-alist))
 (nxml-mode . (
            (indent-tabs-mode . nil)
            ))
 (css-mode . (
            (css-indent-offset . 2)
            ))
 (js-mode . (
             (indent-tabs-mode . nil)
             (js-indent-level . 2)
            ))
 (meson-mode . (
             (indent-tabs-mode . nil)
            ))
)
