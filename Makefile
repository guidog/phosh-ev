APP_ID := mobi.phosh.Ev
RUNTIME_VERSION := $(shell awk '/^runtime-version/ { print $$2 }'  data/$(APP_ID).yaml)

all:
	cargo build

run:
	PYTHONPATH=$(PWD)/../hyundai_kia_connect_api/ cargo run

check:
	appstreamcli validate data/$(APP_ID).metainfo.xml
	desktop-file-validate data/$(APP_ID).desktop

check-flatpak:
	flatpak install --user --include-sdk org.gnome.Platform//$(RUNTIME_VERSION)
	flatpak install --user --include-sdk org.gnome.Sdk//$(RUNTIME_VERSION)
	flatpak-builder --user --install --force-clean repo data/$(APP_ID).yaml
	flatpak run $(APP_ID)

arm64-flatpak:
	flatpak install --user --include-sdk org.gnome.Platform/aarch64/$(RUNTIME_VERSION)
	flatpak install --user --include-sdk org.gnome.Sdk/aarch64/$(RUNTIME_VERSION)
	flatpak-builder --arch=aarch64 --user --repo=repo-aarch64 --force-clean  flatpak-build-aarch64 data/$(APP_ID).yaml
	flatpak build-bundle --arch=aarch64 repo-aarch64 $(APP_ID).flatpak $(APP_ID)
