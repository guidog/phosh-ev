fn main() {
    glib_build_tools::compile_resources(
        &["resources"],
        "resources/mobi.phosh.Ev.gresource.xml",
        "mobi.phosh.Ev.gresource",
    );
}
